var frameModule = require("ui/frame");

function pageLoaded(args) {
    var page = args.object;
    page.bindingContext = { myItems: [{"name": "Big Nodza", "website": "http://safirio.com"}, {"name": "Thomas Jones", "website": "http://tj4lizife.com"}]};
}

exports.pageLoaded = pageLoaded;

exports.nextPage = function() {
  frameModule.topmost().navigate("views/secondaryview/secondaryview");
}
