var fetchModule = require('fetch');

function pageLoaded(args) {
    var page = args.object;
    page.bindingContext = { };
}

exports.pageLoaded = pageLoaded;

exports.httpRequest = function() {
  fetchModule.fetch("https://httpbin.org/get")
  .then(function(response) {
    console.log(JSON.stringify(response));
  }, function(error) {
    console.log(JSON.stringify(error));
  })
}
