var cameraModule = require('camera');

var myImage;

function pageLoaded(args) {
    var page = args.object;
    myImage = page.getViewById("myImage");
    myImage.src = "http://placehold.it/150";
    page.bindingContext = { };
}

exports.pageLoaded = pageLoaded;

exports.tapAction = function() {
  cameraModule.takePicture().then(function(picture) {
    myImage.imageSource = picture;
  });
}
